package br.com.itau;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        int quantidadeSlots = IO.solicitarQuantidadeSlots();

        List<Opcao> opcoes = IO.solicitarOpcoes();

        Maquina maquina = new Maquina(quantidadeSlots, opcoes);

        maquina.jogar();
        
    }
}
