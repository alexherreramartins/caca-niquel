package br.com.itau;

public class Opcao {


    private String nome;
    private int pontuacao;

    public Opcao(String nome, int valor) {
        this.nome = nome;
        this.pontuacao = valor;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public String getNome() {
        return nome;
    }

}
