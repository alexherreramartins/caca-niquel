package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Maquina {

    private Integer quantidade;
    private List<Opcao> opcoes;
    private List<Slot> slots = new ArrayList<>();
    private int pontuacao = 0;
    private List<String> figuras = new ArrayList<>();


    public Maquina(Integer quantidade, List<Opcao> opcoes) {
        this.quantidade = quantidade;
        this.opcoes = opcoes;

        for (int i = 0; i < quantidade; i++) {
            slots.add(new Slot(this.opcoes));
        }
    }

    public List<String> getFiguras() {
        return figuras;
    }

    public void setFiguras(List<String> figuras) {
        this.figuras = figuras;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<Opcao> getOpcoes() {
        return opcoes;
    }

    public void setOpcoes(List<Opcao> opcoes) {
        this.opcoes = opcoes;
    }

    public void jogar() {

        for (Slot slot : this.slots) {
            Opcao resultado = slot.getResultado();

            acumularResultado(resultado);

            IO.imprimirOpcao(resultado);
        }

        validarBonus();

        IO.imprimirPontuacao(String.valueOf(this.pontuacao));
    }

    private void validarBonus() {
        String primeiroElemento = this.figuras.get(0);
        boolean iguais = true;

        for (int i = 1; i < this.figuras.size(); i++) {
            if (!this.figuras.get(i).equals(primeiroElemento)) {
                iguais = false;
                break;
            }
        }

        if (iguais) {
            this.pontuacao = this.pontuacao * 100;
        }
    }

    private void acumularResultado(Opcao opcao) {
        this.pontuacao += opcao.getPontuacao();
        this.figuras.add(opcao.getNome());
    }
}
