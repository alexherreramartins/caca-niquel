package br.com.itau;

import java.util.List;
import java.util.Random;

public class Slot {
    private List<Opcao> opcoes;

    public Slot( List<Opcao> opcoes) {
        this.opcoes = opcoes;
    }

    public List<Opcao> getOpcoes() {
        return opcoes;
    }

    public void setOpcoes(List<Opcao> opcoes) {
        this.opcoes = opcoes;
    }

    public Opcao getResultado(){

        int posicao = new Random().nextInt(this.opcoes.size());

        return this.opcoes.get(posicao);

    }
}
