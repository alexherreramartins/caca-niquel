package br.com.itau;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IO {
    public static void imprimirOpcao(Opcao opcao) {
        System.out.println(opcao.getNome());
    }

    public static void imprimirPontuacao(String pontuacao) {
        System.out.println("Pontuacação final: " + pontuacao);
    }

    public static int solicitarQuantidadeSlots() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Infomr o numero de slots da máquina");
        int quantidade = Integer.parseInt(scanner.nextLine());

        return quantidade;
    }

    public static List<Opcao> solicitarOpcoes() {
        Scanner scanner = new Scanner(System.in);
        List<Opcao> opcoes = new ArrayList<>();

        System.out.println("Infomr o numero de opcoes por slot da máquina");
        int quantidade = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i < quantidade; i++) {
            System.out.println("Nome da figura " + String.valueOf(i + 1) + ":");
            String nome = scanner.nextLine();

            System.out.println("Valor da figura " + String.valueOf(i + 1) + ":");
            String valor = scanner.nextLine();

            opcoes.add(new Opcao(nome, Integer.parseInt(valor)));
        }

        return opcoes;
    }
}
